# Portfolio - FrontEnd Service

This backend service was developed to support the Full Stack crash course final project, providing the web client interfae using a responsive format. Built using [Angular CLI](https://github.com/angular/angular-cli) version 15.0.1.

## Deployment

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The application will automatically reload if you change any of the source files.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Demo

A working [DEMO](https://portfolio-frontend-afe89.web.app) is available for testing purposes. This is using Firebase free TIER subscriptions, this is subject to service standby and/or shutdown per license agreement without notice.

## Observations

Due to free tier limitations, the backend turns into stand-by mode quite frequently. The web service includes a "loading" feature which will notice the user until the backend is able to respond to API requests. Please be patient and wait until data is loaded once logged in.

## License

[Apache License 2.0](https://www.apache.org/licenses/LICENSE-2.0)

## Related

The Angular BeckEnd service can be found [HERE](https://gitlab.com/ch3ch3ni0/portfolio-backend).

## Support

To report issues or share new ideas, please create an issue within this repository.

## Authors

- [@ch3ch3ni0](https://gitlab.com/ch3ch3ni0)