export class Profile {
    name: string;
    title: string;
    location: string;
    about: string;
  
    constructor(name: string, title: string, location: string, about: string) {
      this.name = name;
      this.title = title;
      this.location = location;
      this.about = about;
    }
  }