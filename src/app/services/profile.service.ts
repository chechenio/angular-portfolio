import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environments';
import { Profile } from '../profile';
import { Observable, BehaviorSubject, throwError, catchError, tap } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ProfileService {

  private profileUrl = environment.base_url+"/api/v1/public/getprofile?id="+environment.user_id;

  private profile: Profile = new Profile("", "", "", "");

  constructor(private http: HttpClient) { }

  public getProfile(): Observable<Profile> {
    return this.http.get<Profile>(this.profileUrl).pipe(
      tap(response => {
        this.profile = response;
      }),
      catchError(error => {
        console.log('Error retrieving profile');
        return throwError(error);
      })
    );
  }

  public getProfileValue(): Profile {
    // console.log("assign value");
    // console.log(this.profile);
    return this.profile;
  }

}
