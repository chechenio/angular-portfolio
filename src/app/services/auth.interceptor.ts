import { HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Router } from "@angular/router";
import { Observable, throwError, catchError } from "rxjs";
import { AuthenticationService } from '../services/authentication.service'

// @Injectable()
// export class AuthInterceptor implements HttpInterceptor {

//   constructor(private authService: AuthenticationService, private router: Router) {}

//   intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
//     const token = this.authService.GetToken();

//     if (token) {
//         console.log("Bearer token added to header")
//         const authReq = req.clone({
//         headers: req.headers.set('Authorization', 'Bearer ' + token)
//       });
//       return next.handle(authReq);
//     }
//     console.log("token not available")
//     console.log(req.headers)
//     return next.handle(req);
//   }
// }

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  constructor(private authService: AuthenticationService, private router: Router) {}

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const token = this.authService.GetToken();

    if (token) {
      //console.log("Bearer token added to header");
      const authReq = req.clone({
        headers: req.headers.set('Authorization', 'Bearer ' + token)
      });
      return next.handle(authReq).pipe(
        catchError((error) => {
          // if (error.status === 403 || error.status === 401) {
          //   console.log("Session expired or invalid");
          //   this.authService.Logout();
          //   this.router.navigate(['/login']);
          // }
          return throwError(error);
        })
      );
    }
    console.log("token not available");
    console.log(req.headers);
    return next.handle(req);
  }
}