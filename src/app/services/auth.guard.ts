import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthenticationService } from './authentication.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor(private AuthenticationService: AuthenticationService,
    private router:Router){}

    canActivate(
      next: ActivatedRouteSnapshot,
      state: RouterStateSnapshot
    ): boolean {
      const token = this.AuthenticationService.GetToken();
  
      if (token && this.AuthenticationService.IsAuthenticated()) {
        return true;
      }
  
      // additional logic to check if the token is valid
      this.router.navigate(['/login']);
      return true;
    }   
  
}
