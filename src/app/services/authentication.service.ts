import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environments';
import { BehaviorSubject, Observable } from 'rxjs';
import jwt_decode from 'jwt-decode';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  
  currentUserSubject: BehaviorSubject<any>;
  constructor(private http:HttpClient) { 
    console.log("Auth Service Running")
    this.currentUserSubject = new BehaviorSubject<any>(JSON.parse(sessionStorage.getItem('currentUser')||'{}'));
  }

  public InitSession(email: string, password: string):Observable<any>{
    const body = {"email":email, "password": password};
    console.log("InitSession");
    return this.http.post(environment.base_url+"/api/v1/auth/authenticate", body);
  }

  public CreateUser(email: string, password: string, firstname: string, lastname: string):Observable<any>{
    const body = {"email":email, "password": password, "firstname": firstname, "lastname": lastname};
    return this.http.post(environment.base_url+"/api/v1/auth/register", body);
  }

  public IsAuthenticated(): boolean {
    const token = localStorage.getItem('jwtToken');
    const currentTime = new Date().getTime() / 1000; // Divide by 1000 to convert to seconds
    const decodedToken = this.DecodeToken();
    if (decodedToken!== null && decodedToken.exp > currentTime ){
      return true;
    }else{
      localStorage.removeItem("jwtToken")
      return false;
    }
  }

  public IsAdmin(): boolean {
    const decodedToken = this.DecodeToken();
    if (decodedToken !== null && decodedToken.role == "ADMIN"){
      return true;
    }else{
      return false;
    }
  }

  public SetToken(jwtToken: string) {
    localStorage.setItem('jwtToken', jwtToken);
  }

  public GetToken(): string{
    return localStorage.getItem('jwtToken') as string;
  }

  public DecodeToken(){
    const token = this.GetToken();
    if (token !== null){
      const decodedToken: any = jwt_decode(token);
      return decodedToken;
    }else{
      return null;
    }
    }

    public Logout():void {
      localStorage.removeItem('jwtToken');
    }
}
