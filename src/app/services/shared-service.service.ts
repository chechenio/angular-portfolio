import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SharedServiceService {

  isLoading: boolean = false;

  setLoadingStatus(status: boolean) {
    this.isLoading = status;
  }

  getLoadingStatus(): boolean {
    return this.isLoading;
  }

  constructor() { }
}
