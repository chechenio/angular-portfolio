import { Component, OnInit } from '@angular/core';
// importamos las librerias de formulario que vamos a necesitar
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthenticationService } from '../services/authentication.service';
import { Router } from '@angular/router';
import { NgbModal,NgbActiveModal  } from '@ng-bootstrap/ng-bootstrap';

interface BruteforceRecord {
  [email: string]: {
    attempts: number,
    lastAttemptAt: Date
  }
}

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})

export class LoginComponent implements OnInit {
  signinform: FormGroup;
  signupform: FormGroup;

  errormessage: string = '';
  error = false;
  
  bruteforceRecords: BruteforceRecord = {};
  MAX_ATTEMPTS = 5
  //Bruteforce rate limit in seconds
  RATE_LIMIT_TIME = 5*60*1000;
  
  loading: boolean = false

  // Inyectar en el constructor el formBuilder
  constructor(private formBuilder: FormBuilder, private authService:AuthenticationService, private route:Router, private modal: NgbModal){ 
    ///Creamos el grupo de controles para el formulario de signin y signup
    this.signinform= this.formBuilder.group({
      password:['',[Validators.required]],
      email:['', [Validators.required, Validators.email,]],
   })

   this.signupform= this.formBuilder.group({
    password:['',[Validators.required, Validators.minLength(8),
      Validators.pattern('(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&].{8,}')]],
    email:['', [Validators.required, Validators.email]],
    firstname:['', [Validators.required]],
    lastname:['', [Validators.required]]
    })

  }

  ngOnInit() {}

  //Signin Getters

  get SigninPassword(){
    return this.signinform.get("password");
  }
 
  get SigninEmail(){
   return this.signinform.get("email");
  }

  get SigninPasswordValid(){
    return this.SigninPassword?.touched && !this.SigninPassword?.valid;
  }

  get SigninEmailValid() {
    return false
  }

  //Signup Getters

  get SignupPassword(){
    return this.signupform.get("password");
  }
 
  get SignupEmail(){
   return this.signupform.get("email");
  }

  get SignupFirstname(){
    return this.signupform.get("firstname");
  }

  get SignupLastname(){
    return this.signupform.get("lastname");
  }

  get SignupPasswordValid(){
    return this.SignupPassword?.touched && !this.SignupPassword?.valid;
  }

  get SignupEmailValid() {
    return false
  }
  
 

  onSubmit(event: Event, formType: string){
    // Detenemos la propagación o ejecución del compotamiento submit de un form
    event.preventDefault; 
 
    const modalRef = this.modal.open(MyModalComponent, { centered: true });
    modalRef.componentInstance.message = 'Request sent to backend...Please wait.';
    this.loading = true;

    if (formType === 'signin'){

      const now = new Date();
      let email = this.signinform.controls["email"].value;
      let record = this.bruteforceRecords[email];

      // Reset last failed attempt is > RATE_LIMIT_TIME
      if (record && now.getTime() - record.lastAttemptAt.getTime() > this.RATE_LIMIT_TIME) {
        console.log(record.lastAttemptAt.getTime());
        console.log(record.lastAttemptAt);

        console.log(now.getTime());
        console.log(now);

        this.bruteforceRecords[email].attempts = 0;
      }
    
      // Check if user has exceeded rate limit
      if (record && record.attempts >= this.MAX_ATTEMPTS && (now.getTime() - record.lastAttemptAt.getTime()) < this.RATE_LIMIT_TIME) {
        console.log("User has exceeded rate limit");
        this.errormessage = "Too many failed login attempts. Please try agian in 5 minutes."
        this.error = true;
        modalRef.close();
        return;
      }

      console.log("Signin Request for user "+ this.signinform.controls["email"].value);
      if (this.signinform.valid){
          this.authService.InitSession(this.signinform.controls["email"].value, 
          this.signinform.controls["password"].value).subscribe(response=>{
          console.log("DATA:"+ JSON.stringify(response));
          modalRef.close();
          this.loading = false;
          this.authService.SetToken(response.token);
          this.authService.DecodeToken();
          this.route.navigate(['/home']);
        }, error => {
          if (error.status === 403){          
            console.log("Authentication 403 request");
            // Update bruteforce record
            if (record) {
              // User has already exceeded rate limit, increment attempts
              this.bruteforceRecords[email].attempts++;
              this.bruteforceRecords[email].lastAttemptAt= now;         
            } else {
              // Reset attempts and update last attempt time
              this.bruteforceRecords[email] = { attempts: 1, lastAttemptAt: now };
            }
            console.log("Current User Bruteforce Record");
            console.log(this.bruteforceRecords[email]); 
            this.errormessage = "User and/or password are incorrect, please try again."
            this.error = true;
          }else{
            console.log("Error connecting to API Service");
          }
          modalRef.close();
          this.loading = false;
        }); 
      }else{
        // Corremos todas las validaciones para que se ejecuten los mensajes de error en el template     
        this.signinform.markAllAsTouched(); 
      }
    }

    if (formType === 'signup'){
      console.log("Signup Request for user "+ this.signupform.controls["email"].value);
      if (this.signupform.valid){
          this.authService.CreateUser(this.signupform.controls["email"].value,
          this.signupform.controls["password"].value,
          this.signupform.controls["firstname"].value,
          this.signupform.controls["lastname"].value,).subscribe(response=>{
          console.log("DATA:"+ JSON.stringify(response));
          this.authService.SetToken(response.token);
          this.authService.DecodeToken();
          
          console.log(this.authService.IsAuthenticated());

          this.route.navigate(['/home'])
        }, error => {
           if (error.status === 409){
              this.error=true;
              this.errormessage = 'User already exists';
              throw new Error('User already exists');
           }else{
              this.error=true;
              this.errormessage = 'Unexpected error, contact IT administrator';
              throw new Error('Unexpected error, contact IT administrator');
           }
        }); 
      }else{
        // Corremos todas las validaciones para que se ejecuten los mensajes de error en el template     
        this.signupform.markAllAsTouched(); 
      }
    }

 
  }

  button_status = "SignUp";  
    
  Sign2Register() {  
    if (this.button_status == "SignIn"){
      this.button_status = "SignUp"
    }else{
      this.button_status = "SignIn"
    }
  }  

  get status(){
    return this.button_status;
  }

}

@Component({
  selector: 'my-modal',
  template: `
    <div class="modal-header">
      <h5 class="modal-title">{{ message }}</h5>
    </div>
    <div class="modal-body">
      <p>Backend could take a few minutes to start if inactive....</p>
    </div>
  `
})
export class MyModalComponent {
  message: string="";
  constructor(public activeModal: NgbActiveModal) {}
}