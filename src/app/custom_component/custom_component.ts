import {Component} from '@angular/core';

@Component({
    selector: 'hello-world',
    templateUrl: './custom_component.html',
    styleUrls: ['./custom_component.css']

})

export class HelloWorld {
    title = "Welcome to Angular";
}