import { Component } from '@angular/core';
import { AuthenticationService } from '../services/authentication.service';
import { Location } from '@angular/common';

@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.css']
})
export class ToolbarComponent {

  constructor(private AuthenticationService: AuthenticationService, private location: Location) {}

  username: string = '';
  
  ngOnInit(): void {
    if (this.IsAuthenticated()) {
      this.username = this.AuthenticationService.DecodeToken().firstname + " " + this.AuthenticationService.DecodeToken().lastname;
    }
  }

  public IsAuthenticated(): boolean{
    let auth : boolean = this.AuthenticationService.IsAuthenticated();
    if (auth === true){
      this.username = this.AuthenticationService.DecodeToken().firstname + " " + this.AuthenticationService.DecodeToken().lastname;
    }else{
      this.username = "";
    }
    return auth;
  }

  public Logout(): void {
    this.AuthenticationService.Logout();
    this.location.go('/');
    this.location.replaceState('/');
    window.location.reload();
  }
}
