import { HttpClient } from '@angular/common/http';
import { Component, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { environment } from '../../../environments/environments';
import { AuthenticationService } from '../services/authentication.service';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { Location } from '@angular/common';
import { compileFactoryFunction } from '@angular/compiler';
import { SharedServiceService } from '../services/shared-service.service';

export interface Project {
  name: string;
  company: string;
  description: string;
  id: string;
}

@Component({
  selector: 'app-projects',
  templateUrl: './projects.component.html',
  styleUrls: ['./projects.component.css']
})
export class ProjectsComponent {

  projects: any[] = [];
  isEditMode = false;
  @ViewChild('myModal') myModal: any;
  project: Project = { name: '', company: '', description: '', id: '' };

  constructor(private http: HttpClient, private AuthenticationService: AuthenticationService, private router: Router, private modalService: NgbModal, private toastr: ToastrService, private sharedService: SharedServiceService) { }

  ngOnInit(): void {
    this.sharedService.setLoadingStatus(true);
    this.http.get<any[]>(environment.base_url+"/api/v1/public/getallprojects").subscribe(
      data => {
        if(data && data.length >0) {
          this.projects = data;
        }
      },
      error => {
        console.log(error);
      }
    );
    this.sharedService.setLoadingStatus(false);
  }

  toggleEditMode() {
    this.isEditMode = !this.isEditMode;
  }

  public IsAuthenticated(){
    return this.AuthenticationService.IsAuthenticated();
  }

  public IsAdmin(){
    return this.AuthenticationService.IsAdmin();
  }

  openModal(pid: string,pname: string,pcompany: string,pdescription:string){
    if (pid === 'none'){
      this.project = {
        id: 'none',
        name: '',
        company: '',
        description: ''
      }
    }else{
      this.project = {
        id: pid,
        name: pname,
        company: pcompany,
        description: pdescription
      } 
    }
    this.modalService.open(this.myModal, {centered:true});
  }

  addProject(form: any) {
    const body = {"name":this.project.name, "company": this.project.company, "description": this.project.description};
    if (this.project.id == 'none'){
      this.http.post(environment.base_url+"/api/v1/addproject", body, { responseType: 'text' }).subscribe(
        (res: any) => {
          this.toastr.success('Formulario enviado exitosamente');
          form.reset();
          this.modalService.dismissAll();
          this.ngOnInit();
        },
        (error: any) => {
          this.toastr.error('Error al enviar el formulario');
        }
      );
    }else{
      console.log(body);
      this.http.patch(environment.base_url+"/api/v1/updateproject?id="+this.project.id, body, { responseType: 'text' }).subscribe(
        (res: any) => {
          this.toastr.success('Formulario actualizado exitosamente');
          form.reset();
          this.modalService.dismissAll();
          this.ngOnInit();
        },
        (error: any) => {
          this.toastr.error('Error al actualizar el formulario');
        }
      );
    }
  }

  deleteProject(id){
    this.http.delete(environment.base_url+"/api/v1/deleteproject?id="+id, { responseType: 'text' }).subscribe(
      res => {
        console.log(res);
        this.ngOnInit();
      },
      res => {
        console.error(res);
      }
    );
  }
}
