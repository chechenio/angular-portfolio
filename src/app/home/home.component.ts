import { Component } from '@angular/core';
import { AuthenticationService } from '../services/authentication.service';
import { NgbModal,NgbActiveModal  } from '@ng-bootstrap/ng-bootstrap';
import { SharedServiceService } from '../services/shared-service.service';
import { share } from 'rxjs';



@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent {

  constructor(private AuthenticationService: AuthenticationService, private modal: NgbModal, private sharedService: SharedServiceService) {}

  isEditMode = false;

  ngOnInit(): void {

    const modalRef = this.modal.open(MyModalComponent, { centered: true });
    modalRef.componentInstance.message = 'Request sent to backend...Please wait.';

    setInterval(() => {
      if (! this.sharedService.getLoadingStatus() ) {
        modalRef.close();
      }
    }, 1000); // comprobar cada 1000ms
  }

  toggleEditMode() {
    this.isEditMode = !this.isEditMode;
  }

  public IsAuthenticated(){
    return this.AuthenticationService.IsAuthenticated();
  }

}

@Component({
  selector: 'my-modal',
  template: `
    <div class="modal-header">
      <h5 class="modal-title">{{ message }}</h5>
    </div>
    <div class="modal-body">
      <p>Backend could take a few minutes to start if inactive....</p>
    </div>
  `
})
export class MyModalComponent {
  message: string="";
  constructor(public activeModal: NgbActiveModal) {}
}