import { Component, ViewChild } from '@angular/core';
import { environment } from '../../../environments/environments';
import { AuthenticationService } from '../services/authentication.service';
import { ProfileService } from '../services/profile.service';
import { Profile } from '../profile';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { HttpClient } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import { SharedServiceService } from '../services/shared-service.service';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.css']
})
export class AboutComponent {

  @ViewChild('myModal') myModal: any;
  profile: Profile = new Profile ("","","","");
  isEditMode = false;
  textInput: string = "";

  constructor(private AuthenticationService: AuthenticationService, private profileService: ProfileService, private modalService: NgbModal, private http : HttpClient, private toastr: ToastrService, private sharedService: SharedServiceService ) {}

  toggleEditMode() {
    this.isEditMode = !this.isEditMode;
  }

  public IsAuthenticated(){
    return this.AuthenticationService.IsAuthenticated();
  }

  public IsAdmin(){
    return this.AuthenticationService.IsAdmin();
  }

  ngOnInit(): void {
    this.getProfile();
  }

  getProfile(): void {
    this.sharedService.setLoadingStatus(true);
    this.profileService.getProfile().subscribe((profile) => {
      this.profile = this.profileService.getProfileValue();
    });
    this.sharedService.setLoadingStatus(false);
  }

  openModal(){
    this.textInput = this.profile.about;
    this.modalService.open(this.myModal, {centered:true});
  }

  updateAbout(form: any){
    const body = {"name":this.profile.name, "title": this.profile.title, "location": this.profile.location, "about":this.textInput};
      this.http.patch(environment.base_url+"/api/v1/updateprofile?id="+environment.user_id, body, { responseType: 'text' }).subscribe(
        (res: any) => {
          this.toastr.success('Formulario enviado exitosamente');
          form.reset();
          this.modalService.dismissAll();
          this.ngOnInit();
        },
        (error: any) => {
          this.toastr.error('Error al enviar el formulario');
        }
      );
  }
}
