import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title: string = 'portfolio';
  users: string[] = ['Jose','Pepe','Ramiro','Lalo']

  submitForm(newUser){
    console.log(newUser.value);
    newUser.value='';
    newUser.focus();
    return false; //cancele el refresh del site

  }

}
