import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from '../login/login.component';
import { HomeComponent } from '../home/home.component';
import { AuthGuard } from '../services/auth.guard'
const routes: Routes = [
  {path: "login", component: LoginComponent },
  //Use AuthGuard to restrict access for non active sessions.
  {path: "home", component: HomeComponent, canActivate: [AuthGuard] },
  //{path: "home", component: HomeComponent},
  {path: "", redirectTo:"home",pathMatch:'full' },
  {path: "**", redirectTo:"home",pathMatch:'full' }
];


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forRoot(routes)
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
