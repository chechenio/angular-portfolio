import { Component, Input } from '@angular/core';
import {AppComponent} from '../app.component'

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent {
  @Input() nameUser:string="";
  age: number;

  constructor() {
    this.age = 28;
  }

  sayHello() {
    alert('Hello')
  }

  test= new AppComponent()

  deleteUser(user) {
    for (let i=0; i<this.test.users.length; i++) {
      if (user == this.test.users[i]) {
        this.test.users.splice(i,1);
      }
      console.log(this.test.users)
    }
  }
}
