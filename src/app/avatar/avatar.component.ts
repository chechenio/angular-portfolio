import { Component } from '@angular/core';
import { ProfileService } from '../services/profile.service';
import { Profile } from '../profile';

interface user {  
  fullname: string;  
  email: String;  
  linkedin: String;
}  

@Component({
  selector: 'app-avatar',
  templateUrl: './avatar.component.html',
  styleUrls: ['./avatar.component.css']
})

export class AvatarComponent {

    profile: Profile = new Profile ("","","","");

    constructor(private profileService: ProfileService) {}

    ngOnInit(): void {
      this.getProfile();
    }
  
    getProfile(): void {
      this.profileService.getProfile().subscribe((profile) => {
        this.profile = this.profileService.getProfileValue();
      });
  }

}
