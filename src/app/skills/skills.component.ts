import { Component, ViewChild } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environments';
import { AuthenticationService } from '../services/authentication.service';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { SharedServiceService } from '../services/shared-service.service';

export interface Skill {
  id: string;
  name: string;
  confidence: string
}

@Component({
  selector: 'app-skills',
  templateUrl: './skills.component.html',
  styleUrls: ['./skills.component.css']
})

export class SkillsComponent {

  constructor (private http: HttpClient, private AuthenticationService: AuthenticationService, private modalService: NgbModal, private toastr: ToastrService, private sharedService: SharedServiceService){}

  skills: any[] = [];
  @ViewChild('myModal') myModal: any;
  skill: Skill = { name: '', confidence: '', id: '' };

  ngOnInit(): void {
    this.sharedService.setLoadingStatus(true);
    this.http.get<any[]>(environment.base_url+"/api/v1/public/getallskills").subscribe(
      data => {
        if(data && data.length >0) {
          this.skills = data;
        }
      },
      error => {
        console.log(error);
      }
    );
    this.sharedService.setLoadingStatus(false);
  }
  

  public IsAuthenticated(){
    return this.AuthenticationService.IsAuthenticated();
  }

  public IsAdmin(){
    return this.AuthenticationService.IsAdmin();
  }

  openModal(sid: string,sname: string,sconfidence: string){
    if (sid === 'none'){
      this.skill = {
        id: 'none',
        name: '',
        confidence: ''
      }
    }else{
      this.skill = {
        id: sid,
        name: sname,
        confidence: sconfidence
      } 
    }
    this.modalService.open(this.myModal, {centered:true});
  }

  addSkill(form: any) {
    const body = {"name":this.skill.name, "confidence": this.skill.confidence};
    if (this.skill.id == 'none'){
      this.http.post(environment.base_url+"/api/v1/addskill", body, { responseType: 'text' }).subscribe(
        (res: any) => {
          this.toastr.success('Formulario enviado exitosamente');
          form.reset();
          this.modalService.dismissAll();
          this.ngOnInit();
        },
        (error: any) => {
          this.toastr.error('Error al enviar el formulario');
        }
      );
    }else{
      console.log(body);
      this.http.patch(environment.base_url+"/api/v1/updateskill?id="+this.skill.id, body, { responseType: 'text' }).subscribe(
        (res: any) => {
          this.toastr.success('Formulario actualizado exitosamente');
          form.reset();
          this.modalService.dismissAll();
          this.ngOnInit();
        },
        (error: any) => {
          this.toastr.error('Error al actualizar el formulario');
        }
      );
    }
  }

  deleteSkill(id){
    this.http.delete(environment.base_url+"/api/v1/deleteskill?id="+id, { responseType: 'text' }).subscribe(
      res => {
        console.log(res);
        this.ngOnInit();
      },
      res => {
        console.error(res);
      }
    );
  }

}
