import { Component, ViewChild } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environments';
import { AuthenticationService } from '../services/authentication.service';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { SharedServiceService } from '../services/shared-service.service';

export interface Job {
  id: string;
  company: string;
  start: Date;
  end: Date;
  role: string;
  description: string;
  logo: string;             //base64 image
  logo_ext: string;
  location: string
}

@Component({
  selector: 'app-job',
  templateUrl: './job.component.html',
  styleUrls: ['./job.component.css']
})

export class JobComponent {

  constructor(private http: HttpClient, private modalService: NgbModal, private toastr: ToastrService, private AuthenticationService: AuthenticationService, private sharedService: SharedServiceService){}

  jobs: Job[] = [];
  @ViewChild('myModal') myModal: any;
  job:Job = {id:"", company:"", start:new Date(""), end:new Date(""), role:"", description:"", logo:"", logo_ext:"", location:""}

  public IsAuthenticated(){
    return this.AuthenticationService.IsAuthenticated();
  }

  public IsAdmin(){
    return this.AuthenticationService.IsAdmin();
  }

  openModal(jid: string,jcompany: string,jrole: string,jstart: any, jend: any, jdescription: string, jlocation:string, jlogo:string, jext: string){
    if (jid === 'none'){
      this.job = {
        id: 'none',
        company: '',
        role: '',
        start:new Date(''),
        end:new Date(''),
        description:'',
        logo:'',
        logo_ext:'',
        location:''

      }
    }else{
      this.job = {
        id: jid,
        company: jcompany,
        role: jrole,
        start: jstart,
        end: jend,
        description: jdescription,
        logo:jext+jlogo,
        logo_ext:jext,
        location:jlocation
      } 
    }
    this.modalService.open(this.myModal, {centered:true});
  }

  ngOnInit(): void {
    this.sharedService.setLoadingStatus(true);
    this.http.get<any[]>(environment.base_url+"/api/v1/public/getallexperience").subscribe(
      data => {
        if (data && data.length > 0) {
          this.jobs = data;
          this.sortJobs();
        } else {
          this.jobs = [];
        }
      },
      error => {
        console.log(error);
      }
    );
    this.sharedService.setLoadingStatus(false);
  }
  

  addJob(form: any){
    const match = this.job.logo.match(/^data:image\/(\w+);base64,/);
    //let extension = match ? match[1] : '';
    let extension:string = '';
    let imageData: string = '';
    if (match != null){
      extension = match[0];
      imageData = this.job.logo.substring(match[0].length);
    }
    const body = {"company":this.job.company, 
                  "role": this.job.role, 
                  "description": this.job.description, 
                  "start": this.job.start, 
                  "end":this.job.end,
                  "logo":imageData,
                  "logo_ext":extension,
                  "location":this.job.location};
    console.log(body);
    if (this.job.id == 'none'){
      this.http.post(environment.base_url+"/api/v1/addexperience", body, { responseType: 'text' }).subscribe(
        (res: any) => {
          this.toastr.success('Formulario enviado exitosamente');
          form.reset();
          this.modalService.dismissAll();
          this.ngOnInit();
        },
        (error: any) => {
          this.toastr.error('Error al enviar el formulario');
        }
      );
    }else{
      this.http.patch(environment.base_url+"/api/v1/updateexperience?id="+this.job.id, body, { responseType: 'text' }).subscribe(
        (res: any) => {
          this.toastr.success('Formulario actualizado exitosamente');
          form.reset();
          this.modalService.dismissAll();
          this.ngOnInit();
        },
        (error: any) => {
          this.toastr.error('Error al actualizar el formulario');
        }
      );
    }
  }

  onFileSelected(event: any) {
    const file: File = event.target.files[0];
    let error_message: string = "";

    // Reset error message if present from previous input
    const errorMessage = document.getElementById('error-message');
    if (errorMessage !== null) {
      errorMessage.innerHTML = '';
      errorMessage.classList.add('d-none');
    }
  
    // Image size validation (2MB)
    const fileSizeInMB = file.size / (1024 * 1024);
    if (fileSizeInMB > 2) {
      error_message = 'El archivo seleccionado es demasiado grande. Por favor, seleccione un archivo menor a 2MB.';
    }
  
    // Image type validation (jpeg,png,gif)
    const fileType = file.type;
    if (fileType !== 'image/jpeg' && fileType !== 'image/png' && fileType !== 'image/gif') {
      error_message = 'El archivo seleccionado no es una imagen válida. Por favor, seleccione un archivo JPEG, PNG o GIF.';
    }
  
    // Image res validation (400x400px)
    const image = new Image();
    image.src = URL.createObjectURL(file);
    const width = image.width;
    const height = image.height;
    if (width > 400 || height > 400) {
      error_message = 'El archivo seleccionado es demasiado grande. Por favor, seleccione una imagen con un tamaño máximo de 400x400 píxeles.';
    }

    image.onload = () => {
      const width = image.width;
      const height = image.height;
      if (width > 400 || height > 400) {
        error_message = 'El archivo seleccionado es demasiado grande. Por favor, seleccione una imagen con un tamaño máximo de 400x400 píxeles.';
      }
      if (error_message != ""){
        // Return error message in div id=error-message
        const errorMessage = document.getElementById('error-message');
        if (errorMessage !== null) {
          errorMessage.innerHTML = error_message;
          errorMessage.classList.remove('d-none');
          return;
        }
      }
      // Accept image if all checks are successful
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => {
        this.job.logo = reader.result as string;
      };
    };
  }
    

  sortJobs(){
    this.jobs.sort((a, b) => {
      const aEndDate = a.end ? new Date(a.end).getTime() : Infinity;
      const bEndDate = b.end ? new Date(b.end).getTime() : Infinity;
      if (aEndDate === bEndDate) {
        const aStartDate = a.start ? new Date(a.start).getTime() : Infinity;
        const bStartDate = b.start ? new Date(b.start).getTime() : Infinity;
        return aStartDate - bStartDate;
      }
      return bEndDate - aEndDate;
    });
  }

  deleteJob(id){
    this.http.delete(environment.base_url+"/api/v1/deleteexperience?id="+id, { responseType: 'text' }).subscribe(
      res => {
        console.log(res);
        this.ngOnInit();
      },
      res => {
        console.error(res);
      }
    );
  }

}
